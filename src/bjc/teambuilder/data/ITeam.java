/**
 * TeamBuilder 2.0 - Team play assist
 * Created 15/08/15
 *
 * Written by Benjamin Culkin
 * This code is released into the public domain
 */
package bjc.teambuilder.data;

import java.util.Set;

/**
 * A team of one or more people, to be matched against other teams
 * @author ben
 *
 */
public interface ITeam {
	/**
	 * Get the # of people in this team
	 * @return # of people in this team
	 */
	int getCount();
	
	/**
	 * Get the people on this team
	 * @return Set of people on this team
	 */
	Set<String> getNames();
	
}
