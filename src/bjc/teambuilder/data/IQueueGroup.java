/**
 * TeamBuilder 2.0 - Team play assist
 * Created 15/08/15
 *
 * Written by Benjamin Culkin
 * This code is released into the public domain
 */
package bjc.teambuilder.data;

import java.util.Set;

/**
 * A group containing one or more people who should be teamed together
 * @author ben
 *
 */
public interface IQueueGroup {
	/**
	 * Get the # of people in this group
	 * @return # of people in this group
	 */
	int getCount();
	
	/**
	 * Get the people in this group
	 * @return Set of people in this group
	 */
	Set<String> getNames();
}
